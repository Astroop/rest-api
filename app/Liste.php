<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liste extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lists';

     /**
     * Get the items for the list.
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }
}