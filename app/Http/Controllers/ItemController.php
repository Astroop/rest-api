<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $items = item::with('list')->get()->toJson();
        return response()->json(
            $items,
            200
        );
    }

    public function get($id)
    {
        try {
        $item = item::findOrFail($id)->toJson();
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Ressource not found'
            ], 404); 
        }

        return response()->json(
            $item,
            200
        );
    }

    public function store(Request $request, $assignment)
    {
        $item = new item;
        $item->completed = $request->completed;
        $item->text = $request->text;
        $item->liste_id = $assignment;
        $item->save();

        return response()->json([
            'message' => 'Ressource successfully created'
        ], 200);

    }

    public function update(Request $request, $id)
    {
        try {
            $item = item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Ressource not found'
            ], 404); 
        }

        $item->completed = $request->completed ?? $item->completed;
        $item->text = $request->text ?? $item->text;
        $item->liste_id = $request->liste ?? $item->liste_id;
        $item->save();
        
        return response()->json([
            'message' => 'Ressource successfully updated'
        ], 200);
    }


    public function destroy($id)
    {
        try {
            $item = item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Ressource not found'
            ], 404); 
        }

        $item->delete();

        return response()->json([
            'message' => 'Ressource successfully deleted'
        ], 200);
    }
}
