<?php

namespace App\Http\Controllers;

use App\Liste;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $listes = Liste::with('items')->get()->toJson();
        return response()->json(
            $listes,
            200
        );
    }

    public function get($id)
    {
        try {
            $liste = Liste::with('items')->findOrFail($id)->toJson();
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Ressource not found'
            ], 404); 
        }

        return response()->json(
            $liste,
            200
        );
    }

    public function store(Request $request)
    {
        $liste = new Liste;
        $liste->title = $request->title;
        $liste->color = $request->color;
        $liste->save();

        return response()->json([
            'message' => 'Ressource successfully created'
        ], 200);

    }

    public function update(Request $request, $id)
    {
        $liste = Liste::findOrFail($id);
        $liste->title = $request->title;
        $liste->color = $request->color;
        $liste->save();

        return response()->json([
            'message' => 'Ressource successfully updated'
        ], 200);

    }

    public function destroy($id)
    {
        try {
        $liste = Liste::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'Ressource not found'
            ], 404); 
        }
        
        $liste->delete();

        return response()->json([
            'message' => 'Ressource successfully deleted'
        ], 200);
    }
}
