<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * Get the list that owns the items.
     */
    public function list()
    {
        return $this->belongsTo('App\List');
    }
}