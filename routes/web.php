<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return response()->json([
        'name' => 'REST en chien', 
        'description' => 'Todo-list api']);
});

$router->get('/lists', 'ListController@index');
$router->post('/lists', 'ListController@store');
$router->get('lists/{id}', 'ListController@get');
$router->put('/lists/{id}', 'ListController@update');
$router->delete('/lists/{id}', 'ListController@destroy');

$router->get('/items', 'ItemController@index');
$router->get('/items/{id}', 'ItemController@get');
$router->post('/items/{id}', 'ItemController@store');
$router->put('/items/{id}', 'ItemController@update');
$router->delete('/items/{id}', 'ItemController@destroy');
